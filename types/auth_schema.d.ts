/* Schemas based in interface for typescript */

export interface AuthRequest {
  email: string;
  password: string;
}
