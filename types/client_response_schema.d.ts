/* Schemas based in interface for typescript */

export interface ClientResponse {
  names: string;
  lastnames: string;
  created_at?: string;
}
