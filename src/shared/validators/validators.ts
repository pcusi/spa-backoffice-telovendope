import {
  AbstractControl,
  FormControl,
  FormGroup,
  FormArray,
} from '@angular/forms';

export function hasChanges(control: AbstractControl, obj: any): boolean {
  if (control instanceof FormControl) {
    if (Array.isArray(obj)) {
      let valorControl = control.value as Array<any>;
      return (
        obj.length !== valorControl.length ||
        obj.some((v, i) => v !== valorControl[i])
      );
    } else {
      return control.value !== obj;
    }
  }
  if (control instanceof FormGroup) {
    return Object.keys(control.controls).some((key) =>
      hasChanges(control.controls[key], obj[key]),
    );
  }
  if (control instanceof FormArray) {
    return (
      control.controls.length !== obj.length ||
      control.controls.some((c, i) => hasChanges(c, obj[i]))
    );
  }
  return false;
}
