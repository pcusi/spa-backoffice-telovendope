import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpinnerComponent } from './spinner/spinner.component';
import { SidebarComponent } from '@app/admin';
import { NgIconsModule } from '@ng-icons/core';
import {
  heroBuildingStorefront,
  heroShoppingBag,
  heroChartBarSquare,
  heroUsers,
  heroCog6Tooth,
  heroCircleStack,
  heroPencilSquare,
  heroTrash,
  heroXMark,
  heroMagnifyingGlass,
} from '@ng-icons/heroicons/outline';
import { RouterLink, RouterLinkActive } from '@angular/router';
import { TableComponent } from './table/table.component';
import { BrowserModule } from '@angular/platform-browser';
import { TableCellComponent } from './table/table-cell/table-cell.component';
import { DrawerComponent } from './drawer/drawer.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormContainerComponent } from '@shared/form/form-container.component';
import { TableSkeletonComponent } from './table/table-skeleton/table-skeleton.component';

@NgModule({
  declarations: [
    SpinnerComponent,
    SidebarComponent,
    TableComponent,
    TableCellComponent,
    DrawerComponent,
    FormContainerComponent,
    TableSkeletonComponent,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    NgIconsModule.withIcons({
      heroBuildingStorefront,
      heroShoppingBag,
      heroChartBarSquare,
      heroUsers,
      heroCog6Tooth,
      heroCircleStack,
      heroPencilSquare,
      heroMagnifyingGlass,
      heroTrash,
      heroXMark,
    }),
    RouterLinkActive,
    RouterLink,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [
    SpinnerComponent,
    SidebarComponent,
    TableComponent,
    DrawerComponent,
    FormContainerComponent,
    TableSkeletonComponent,
  ],
})
export class SharedModule {}
