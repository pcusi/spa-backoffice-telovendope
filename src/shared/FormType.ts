import { FormControl, FormGroup } from '@angular/forms';

export type FormType<T> = FormGroup<{
  [K in keyof T]: FormControl<T[K]>;
}>;

export type FormErrorResponse = {
  errorCode: number;
  message: string;
  details: string;
  validationErrors: {
    [property: string]: Array<{
      [error: string]: Array<{ [parameter: string]: any }>;
    }>;
  };
};
