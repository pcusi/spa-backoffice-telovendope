import { OnInit, AfterViewInit, Input, Directive } from '@angular/core';
import { FormGroup, AbstractControl } from '@angular/forms';
import { hasChanges } from '@shared/validators/validators';
import { timer } from 'rxjs';

@Directive()
export abstract class PartialFormComponent<T> implements OnInit, AfterViewInit {
  @Input() skipInitialize: boolean | undefined;
  @Input() createsForm: boolean | undefined;
  @Input() loadingForm: boolean | undefined;
  @Input() readonly: boolean | undefined;
  @Input() form: FormGroup | undefined;
  @Input() source: AbstractControl | undefined;
  @Input() entity: T | undefined;
  @Input() prefix: string = '';
  @Input() set controlName(value: string) {
    this._controlName = value;
  }
  get controlName(): string {
    return this._controlName;
  }

  private _errors: any = {};
  private checkErrors: (key: string) => void = () => {};
  target: FormGroup | undefined;

  protected abstract _controlName: string;
  abstract assignControls(): void;
  abstract get defaultEntity(): T;

  protected addControl(key: string, control: AbstractControl): AbstractControl {
    let formControl = this.target!.get(key);
    if (!formControl) {
      formControl = control;
      this.target!.addControl(key, control);
    }
    this.checkErrors(key);
    return formControl;
  }

  protected removeControl(key: string): void {
    if (this.target!.get(key)) {
      this.target!.removeControl(key);
    }
  }

  protected getControl(key: string): AbstractControl {
    return <AbstractControl<any, any>>this.target!.get(key);
  }

  ngOnInit(): void {
    if (!this.entity) {
      this.entity = this.defaultEntity;
    }
    if (!this.form) {
      this.form = new FormGroup({});
      this.target = this.form;
    } else if (this.createsForm) {
      const target = this.form.get(this.controlName) as FormGroup;
      if (target) {
        this.target = target;
      } else {
        this.target = new FormGroup({});
        this.form.addControl(this.controlName, this.target);
      }
    } else {
      this.target = this.form;
    }
    this.ngBeforeAssignControls();
    if (!this.skipInitialize) {
      if (this.source) {
        this.entity = this.source.value;
        if (this.source instanceof FormGroup) {
          this.checkErrors = (key: string) => {
            const control = this.source!.get(key);
            if (control!.invalid) {
              this._errors[key] = control!.errors;
            }
          };
        }
      }
      this.assignControls();
    }
    this.ngAfterInit();
  }

  ngBeforeAssignControls(): void {}

  ngAfterInit(): void {}

  ngAfterViewInit(): void {
    if (this._errors) {
      timer(100).subscribe({
        next: () => {
          Object.keys(this._errors).forEach((key) => {
            const control = this.form!.get(key);
            control!.setErrors(this._errors[key]);
            control!.markAsTouched();
          });
          this._errors = {};
        },
      });
    }
  }

  get hasChanges(): boolean {
    return hasChanges(this.target!, this.entity);
  }
}
