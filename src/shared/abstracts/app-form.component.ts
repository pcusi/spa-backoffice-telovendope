import { OnInit, OnDestroy, AfterViewInit, Directive } from '@angular/core';
import {
  AbstractControl,
  FormGroup,
  FormArray,
  FormControl,
} from '@angular/forms';
import { Subscription } from 'rxjs';

@Directive()
export abstract class AppFormComponent
  implements OnInit, OnDestroy, AfterViewInit
{
  private _subscriptions: Array<Subscription> = [];
  form: FormGroup | undefined;
  submittedForm: boolean = false;
  loadingForm: boolean = false;
  autofocus: boolean = true;
  submitServerMessage: string | undefined;
  unmatchedErrors: { [key: string]: any } = {};

  abstract formId: string;
  abstract initForm(): void;
  abstract submitForm(): void;

  ngOnInit(): void {
    this.initForm();
  }

  ngAfterViewInit(): void {
    if (this.autofocus) {
      this.focusFirstError();
    }
  }
  ngOnDestroy(): void {
    while (this._subscriptions.length) {
      // @ts-ignore
      this._subscriptions.pop().unsubscribe();
    }
  }
  get canSubmit(): boolean {
    this.touchControl(this.form!);
    this.submittedForm = true;
    if (!this.loadingForm) {
      this.submitServerMessage = '';
      if (this.form!.valid) {
        this.loadingForm = true;
        return true;
      } else {
        this.focusFirstError();
      }
    }
    return false;
  }
  focusFirstError(): void {
    const el = document
      .getElementById(this.formId)
      ?.getElementsByClassName('ng-invalid');
    if (el) {
      for (let i = 0; i < el.length; ++i) {
        // @ts-ignore
        const tag = el.item(i).tagName.toLowerCase();
        if (
          tag === 'input' ||
          tag === 'textarea' ||
          tag === 'select' ||
          tag === 'button'
        ) {
          const item: HTMLInputElement = <HTMLInputElement>el.item(i);
          item.focus();
          if (tag !== 'select' && tag !== 'button') {
            item.select();
          }
          break;
        }
      }
    }
  }
  private touchControl(control: AbstractControl): void {
    if (!(control instanceof FormControl)) {
      if (control instanceof FormGroup) {
        Object.keys(control.controls).forEach((key) =>
          // @ts-ignore
          this.touchControl(control.get(key)),
        );
      } else if (control instanceof FormArray) {
        control.controls.forEach((subctrl) => this.touchControl(subctrl));
      }
    }
    control.markAsTouched();
    control.updateValueAndValidity();
  }
}
