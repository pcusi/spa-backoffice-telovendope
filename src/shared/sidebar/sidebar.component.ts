import { Component } from '@angular/core';
import {MenuOption, sidebar} from "@infrastructure/constants/sidebar";

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent {
  protected readonly sidebar: MenuOption[] = sidebar;
}
