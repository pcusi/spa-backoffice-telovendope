import { FormGroup } from '@angular/forms';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { AppFormComponent } from '@shared/abstracts/app-form.component';

@Component({
  selector: 'form-container',
  templateUrl: './form-container.component.html',
  exportAs: 'formContainer',
})
export class FormContainerComponent extends AppFormComponent {
  @Input() formId: string = '';
  @Input() override autofocus: boolean = false;
  @Output() onSubmit: EventEmitter<FormContainerComponent> =
    new EventEmitter<FormContainerComponent>();

  initForm(): void {
    if (!this.form) {
      this.form = new FormGroup({});
    }
  }
  submitForm(): void {
    if (this.canSubmit) {
      this.onSubmit.emit(this);
    }
  }
}
