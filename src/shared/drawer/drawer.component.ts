import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { AppFormComponent } from '@shared/abstracts/app-form.component';
export type DrawerFormField = {
  key: string;
};

@Component({
  selector: 'app-drawer',
  templateUrl: './drawer.component.html',
  styleUrls: ['./drawer.component.scss'],
})
export class DrawerComponent extends AppFormComponent {
  @Input() open: boolean = false;
  @Input() fields: DrawerFormField[] = [];
  @Input() formId: string = '';
  @Input() override autofocus: boolean = false;
  @Output() close: EventEmitter<boolean> = new EventEmitter();
  @Output() onSubmit: EventEmitter<any> = new EventEmitter<any>();
  constructor() {
    super();
  }

  closeDrawer() {
    if (this.open) return this.close.emit(false);
  }

  initForm(): void {
    if (!this.form) {
      this.form = new FormGroup({});
    }
  }
  submitForm(): void {
    if (this.canSubmit) {
      this.onSubmit.emit(this);
    }
  }
}
