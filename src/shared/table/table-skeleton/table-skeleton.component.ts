import { Component } from '@angular/core';

@Component({
  selector: 'app-table-skeleton',
  templateUrl: './table-skeleton.component.html',
  styleUrls: ['./table-skeleton.component.scss'],
})
export class TableSkeletonComponent {
  protected readonly skeletons: number[] = [0, 0, 0, 0];
}
