import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
} from '@angular/core';
import { JsonKeyHeaders, TableHeaders } from '@infrastructure/interfaces/table';
import { ClientResponse } from 'types/client_response_schema';
import {
  INPUT_SELECT_STYLE,
} from '@infrastructure/variants/styles';
import { perPages } from '@infrastructure/constants/select-pages';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class TableComponent implements OnChanges {
  protected readonly perPages: number[] = perPages;
  protected readonly INPUT_SELECT_STYLE: string = INPUT_SELECT_STYLE;

  @Input() currentPage: number = 0;
  @Input() pages: number[] = [];
  @Input() itemsPerPage: number = 0;
  @Input() tableHeaders: TableHeaders[] = [];
  @Input() tableData: any[] = [];
  @Input() loading: boolean = false;
  @Input() tableDataUnfiltered: any[] = [];
  @Input() mapKeyHeaders: JsonKeyHeaders[] = [];
  @Input() filter: string = '';
  @Output() onEditRow: EventEmitter<any> = new EventEmitter<any>();
  @Output() filterQuery: EventEmitter<string> = new EventEmitter<string>();
  @Output() filterItemsPerPage: EventEmitter<number> =
    new EventEmitter<number>();
  @Output() pageChange: EventEmitter<number> = new EventEmitter<number>();

  ngOnChanges({ tableData }: any) {
    const { currentValue } = tableData;

    if (currentValue.length === 0) {
      console.info('holi mundo');
    }
  }

  onEditRow$(value: ClientResponse): void {
    return this.onEditRow.emit(value);
  }
  filterQuery$(filter: string): void {
    return this.filterQuery.emit(filter);
  }
  filterItemsPerPage$(page: number): void {
    return this.filterItemsPerPage.emit(page);
  }
  pageChange$(page: number): void {
    return this.pageChange.emit(page);
  }
  getFirstItemIndex(): number {
    return (this.currentPage - 1) * this.itemsPerPage + 1;
  }
  getLastItemIndex(): number {
    const currentItems: any[] = this.tableData;
    return (this.currentPage - 1) * this.itemsPerPage + currentItems.length;
  }
}
