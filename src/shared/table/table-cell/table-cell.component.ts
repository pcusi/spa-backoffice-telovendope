import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {
  CellJsonData,
  JsonKey,
  JsonKeyHeaders,
} from '@infrastructure/interfaces/table';
import { TableCellType } from '@infrastructure/enum/TableCellType';
import {
  TABLE_OPTION_ACTION,
  TABLE_OPTION_DELETE,
} from '@infrastructure/variants/styles';

@Component({
  selector: 'app-table-cell',
  templateUrl: './table-cell.component.html',
  styleUrls: ['./table-cell.component.scss'],
})
export class TableCellComponent implements OnInit {
  @Input() data: any;
  @Input() type: string = '';
  @Input() json: CellJsonData[] = [];
  @Input() mapKeyHeaders: JsonKeyHeaders = { keys: [] };
  @Output() onEditRow: EventEmitter<any> = new EventEmitter<any>();
  public tableData: any[][] = [];
  protected readonly TableCellType = TableCellType;
  protected readonly TABLE_OPTION_ACTION: string = TABLE_OPTION_ACTION;
  protected readonly TABLE_OPTION_DELETE: string = TABLE_OPTION_DELETE;

  ngOnInit() {
    this.tableData = this.json.map((v: CellJsonData) =>
      v.keys.map((k: JsonKey) => v.data[k.key]),
    );
  }

  editRow$() {
    this.onEditRow.emit(this.data);
  }
}
