import { createFeature, createReducer, on } from '@ngrx/store';
import { ClientResponse } from 'types/client_response_schema';
import {
  getClients,
  loadingClients,
} from '@store/actions/client/client.actions';

export type ClientState = {
  state: {
    loading: boolean;
    clients: ClientResponse[];
  };
};

export const initClientState: ClientState = {
  state: {
    loading: false,
    clients: [],
  },
};

export const clientFeature = createFeature({
  name: 'client',
  reducer: createReducer(
    initClientState,
    on(getClients, ({ state }, { clients }) => {
      return {
        state: {
          loading: false,
          clients: clients,
        },
      };
    }),
    on(loadingClients, ({ state }, { loading }) => {
      return {
        state: {
          ...state,
          loading,
        },
      };
    }),
  ),
});

export const { selectState } = clientFeature;
export const { reducer } = clientFeature;
