import { createFeature, createReducer, on } from '@ngrx/store';
import { authentication, authLoading } from '@store/actions/auth/auth.actions';

export interface AuthState {
  loading: boolean;
}
export const authInitialState: AuthState = {
  loading: false,
};

export const authFeature = createFeature({
  name: 'auth',
  reducer: createReducer(
    authInitialState,
    on(authentication, (state: AuthState) => state),
    on(authLoading, (_, { loading }) => ({
      loading,
    })),
  ),
});
