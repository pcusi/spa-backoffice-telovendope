import { createFeature, createReducer, on } from '@ngrx/store';
import { ClientResponse } from 'types/client_response_schema';
import { modifyClient } from '@store/actions/form/form.actions';
import { defaultClient } from '@infrastructure/constants/default-values';

export interface ReactiveFormState {
  client: ClientResponse;
}
export const reactiveFormState: ReactiveFormState = {
  client: defaultClient,
};
export const reactiveFormFeature = createFeature({
  name: 'form',
  reducer: createReducer(
    reactiveFormState,
    on(modifyClient, (state: ReactiveFormState, client) => ({
      client,
    })),
  ),
});

export const { selectClient } = reactiveFormFeature;
