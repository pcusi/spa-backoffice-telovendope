import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map, switchMap } from 'rxjs';
import { ClientsService } from '@app/service/clients.service';
import { ActionsType } from '@infrastructure/enum/Actions';
import { getClients } from '@store/actions/client/client.actions';
import { ClientResponse } from 'types/client_response_schema';

@Injectable()
export class ClientEffects {
  constructor(
    private _actions$: Actions,
    private _clientService: ClientsService,
  ) {}

  initClients$ = createEffect(() =>
    this._actions$.pipe(
      ofType(ActionsType.INIT_CLIENTS),
      switchMap(() =>
        this._clientService.getClients().pipe(
          map((clients: ClientResponse[]) => {
            return getClients({ clients: clients });
          }),
        ),
      ),
    ),
  );
}
