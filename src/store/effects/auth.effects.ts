import { Injectable } from '@angular/core';
import { AuthService } from '@app/service/auth.service';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map, switchMap } from 'rxjs';
import { authentication, authLoading } from '@store/actions/auth/auth.actions';
import { AuthRequest } from 'types/auth_schema';
import { Router } from '@angular/router';

@Injectable()
export class AuthEffects {
  constructor(
    private _actions$: Actions,
    private _authService: AuthService,
    private _router: Router,
  ) {}

  authentication$ = createEffect(() =>
    this._actions$.pipe(
      ofType(authentication),
      switchMap((action: AuthRequest) =>
        this._authService
          .auth({
            email: action.email,
            password: action.password,
          })
          .pipe(map(() => authLoading({ loading: false }))),
      ),
    ),
  );
}
