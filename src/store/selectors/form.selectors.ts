import { createSelector } from '@ngrx/store';
import { selectClient } from '@store/reducers/form.reducer';
import { ClientResponse } from 'types/client_response_schema';

export const getClient = createSelector(
  selectClient,
  (state: ClientResponse) => state,
);
