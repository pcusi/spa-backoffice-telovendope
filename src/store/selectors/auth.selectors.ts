import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AuthState } from '@store/reducers/auth.reducer';
export const getLoadingState = createFeatureSelector<AuthState>('auth');
export const selectLoading = createSelector(
  getLoadingState,
  (state: AuthState) => state.loading,
);
