import { createAction, props } from '@ngrx/store';
import { ActionsType } from '@infrastructure/enum/Actions';
import { ClientResponse } from 'types/client_response_schema';
export const initClients = createAction(ActionsType.INIT_CLIENTS);
export const getClients = createAction(
  ActionsType.LIST_CLIENTS,
  props<{ clients: ClientResponse[] }>(),
);
export const loadingClients = createAction(
  ActionsType.LOADING_CLIENTS,
  props<{ loading: boolean }>(),
);
