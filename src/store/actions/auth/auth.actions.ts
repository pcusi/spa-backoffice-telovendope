import { createAction, props } from '@ngrx/store';
import { ActionsType } from '@infrastructure/enum/Actions';
import { AuthState } from '@store/reducers/auth.reducer';
import { AuthRequest } from 'types/auth_schema';

export const authentication = createAction(
  ActionsType.AUTH,
  props<AuthRequest>(),
);
export const authLoading = createAction(
  ActionsType.AUTH_LOADING,
  props<AuthState>(),
);
