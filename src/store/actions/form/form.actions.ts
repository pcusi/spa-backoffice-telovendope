import { createAction, props } from '@ngrx/store';
import { ActionsType } from '@infrastructure/enum/Actions';
import { ClientResponse } from 'types/client_response_schema';

export const modifyClient = createAction(
  ActionsType.MODIFY_CLIENT,
  props<ClientResponse>(),
);
