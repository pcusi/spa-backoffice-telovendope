import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root',
})
export class UtilsService {
  constructor() {}
  public static getPages(itemsPerPage: number, items: any[]): number[] {
    const pages: number[] = [];
    const totalPages: number = Math.ceil(items.length / itemsPerPage);

    for (let i: number = 1; i <= totalPages; i++) {
      pages.push(i);
    }

    return pages;
  }
  public static getCurrentItems(
    itemsPerPage: number,
    currentPage: number,
    items: any[],
  ): any[] {
    const startIndex: number = (currentPage - 1) * itemsPerPage;
    const endIndex: number = startIndex + itemsPerPage;

    return items.slice(startIndex, endIndex);
  }
}
