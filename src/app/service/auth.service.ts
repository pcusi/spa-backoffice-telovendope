import { Injectable } from '@angular/core';
import { supabase } from '@infrastructure/container/Container';
import { AuthRequest } from 'types/auth_schema';
import { Storage } from '@infrastructure/enum/Storage';
import { map, mergeMap, Observable, of } from 'rxjs';
import { authLoading } from '@store/actions/auth/auth.actions';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(
    private _store: Store,
    private _router: Router,
  ) {}

  public auth(request: AuthRequest): Observable<boolean> {
    this._store.dispatch(
      authLoading({
        loading: true,
      }),
    );

    return of(1).pipe(
      mergeMap(async () => await supabase.auth.signInWithPassword(request)),
      mergeMap(({ data }) => {
        if (data.session !== null) return this._authenticateUser(data.session);

        return of(false);
      }),
    );
  }
  public getToken(): string | null {
    return localStorage.getItem(Storage.ACCESS_TOKEN);
  }

  private _authenticateUser(data: any): Observable<boolean> {
    localStorage.setItem(Storage.ACCESS_TOKEN, data.access_token);

    return of(1).pipe(
      map(() => this._router.navigateByUrl('/admin/dashboard')),
      map((): boolean => true),
    );
  }
}
