import { Injectable } from '@angular/core';
import { mergeMap, Observable, of } from 'rxjs';
import { supabase } from '@infrastructure/container/Container';
import { ClientResponse } from 'types/client_response_schema';
import { format } from 'date-fns';
import { defaultTo, get } from 'lodash';
import { Store } from '@ngrx/store';
import {
  initClients,
  loadingClients,
} from '@store/actions/client/client.actions';

@Injectable({
  providedIn: 'root',
})
export class ClientsService {
  constructor(private _store: Store) {}
  public createClient(client: ClientResponse): Observable<void> {
    return of(1).pipe(
      mergeMap(
        async () =>
          await supabase
            .from('clients')
            .insert([{ names: client.names, lastnames: client.lastnames }]),
      ),
      mergeMap(async () => this._store.dispatch(initClients())),
    );
  }

  public getClients(): Observable<ClientResponse[]> {
    this._store.dispatch(loadingClients({ loading: true }));

    return of(1).pipe(
      mergeMap(() => supabase.from('clients').select('*')),
      mergeMap(({ data }) => {
        const mapping: ClientResponse[] = data!.map(
          (client: ClientResponse) => ({
            ...client,
            created_at: format(
              new Date(defaultTo(get(client, 'created_at'), '')),
              'yyyy-MM-dd',
            ),
          }),
        );

        return of(mapping);
      }),
    );
  }
}
