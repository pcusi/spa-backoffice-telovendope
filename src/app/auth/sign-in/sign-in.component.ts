import { Component, OnInit } from '@angular/core';
import {
  BTN_STYLE,
  INPUT_FORM_GROUP_STYLE,
  INPUT_LABEL_STYLE,
  INPUT_STYLE,
} from '@infrastructure/variants/styles';
import { FormBuilder, Validators } from '@angular/forms';
import { FormType } from '@shared/FormType';
import { AuthRequest } from 'types/auth_schema';
import { select, Store } from '@ngrx/store';
import { AuthState } from '@store/reducers/auth.reducer';
import { authentication } from '@store/actions/auth/auth.actions';
import { selectLoading } from '@store/selectors/auth.selectors';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss'],
})
export class SignInComponent implements OnInit {
  protected readonly INPUT_STYLE: string = INPUT_STYLE;
  protected readonly INPUT_LABEL_STYLE: string = INPUT_LABEL_STYLE;
  protected readonly INPUT_FORM_GROUP_STYLE: string = INPUT_FORM_GROUP_STYLE;
  protected readonly BTN_STYLE: string = BTN_STYLE;
  public loading: boolean = false;

  constructor(
    private _formBuilder: FormBuilder,
    private _store: Store<AuthState>,
  ) {}

  ngOnInit() {
    this._store.pipe(select(selectLoading)).subscribe((loading: boolean) => {
      this.loading = loading;
    });
  }

  public signInForm: FormType<AuthRequest> =
    this._formBuilder.nonNullable.group({
      email: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });

  onSubmit(): void {
    this._store.dispatch(
      authentication({
        email: this.signInForm.controls.email.value,
        password: this.signInForm.controls.password.value,
      }),
    );
  }
}
