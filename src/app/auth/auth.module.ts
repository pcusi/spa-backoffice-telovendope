import { NgModule } from '@angular/core';
import { CommonModule, NgOptimizedImage } from '@angular/common';
import { AuthComponent } from '@app/auth/auth.component';
import { RouterModule } from '@angular/router';
import { SignInComponent } from '@app/auth/index';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { provideStore } from '@ngrx/store';
import { provideEffects } from '@ngrx/effects';
import { AuthEffects } from '@store/effects/auth.effects';
import { SharedModule } from '@shared/shared.module';
@NgModule({
  declarations: [AuthComponent, SignInComponent],
  imports: [
    CommonModule,
    RouterModule,
    NgOptimizedImage,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  bootstrap: [AuthComponent],
  providers: [provideStore(), provideEffects(AuthEffects)],
})
export class AuthModule {}
