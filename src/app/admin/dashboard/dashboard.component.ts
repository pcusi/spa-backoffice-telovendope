import { Component } from '@angular/core';
import { ADMIN_CHILDREN_TITLE } from '@infrastructure/variants/styles';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent {
  protected readonly ADMIN_CHILDREN_TITLE: string = ADMIN_CHILDREN_TITLE;
}
