import { Component, OnChanges } from '@angular/core';
import {
  INPUT_FORM_GROUP_STYLE,
  INPUT_LABEL_STYLE,
  INPUT_STYLE,
} from '@infrastructure/variants/styles';
import { PartialFormComponent } from '@shared/abstracts/partial-form';
import { ClientResponse } from 'types/client_response_schema';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { defaultClient } from '@infrastructure/constants/default-values';

@Component({
  selector: 'app-add-client',
  templateUrl: './add-client.component.html',
  styleUrls: ['./add-client.component.scss'],
})
export class AddClientComponent
  extends PartialFormComponent<ClientResponse>
  implements OnChanges
{
  protected readonly INPUT_FORM_GROUP_STYLE: string = INPUT_FORM_GROUP_STYLE;
  protected readonly INPUT_LABEL_STYLE: string = INPUT_LABEL_STYLE;
  protected readonly INPUT_STYLE: string = INPUT_STYLE;
  // @ts-ignore
  public target: FormGroup;

  constructor() {
    super();
  }

  protected _controlName: string = 'client';

  ngOnChanges(changes: any) {
    const { currentValue } = changes.entity;

    if (this.target !== undefined) {
      this.target.patchValue({
        names: currentValue.names,
        lastnames: currentValue.lastnames,
      });
    }
  }

  assignControls(): void {
    this.addControl(
      'names',
      new FormControl('', {
        validators: [Validators.required],
      }),
    );
    this.addControl(
      'lastnames',
      new FormControl('', {
        validators: [Validators.required],
      }),
    );
  }

  get names(): FormControl {
    return this.getControl('names') as FormControl;
  }
  get lastnames(): FormControl {
    return this.getControl('lastnames') as FormControl;
  }

  get defaultEntity(): ClientResponse {
    return {
      ...defaultClient,
    };
  }
}
