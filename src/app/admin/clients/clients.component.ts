import { Component, OnInit } from '@angular/core';
import { ClientsService } from '@app/service/clients.service';
import { tableClientHeaders } from '@infrastructure/constants/headers';
import { mapClientHeaders } from '@infrastructure/constants/map-headers';
import { JsonKeyHeaders, TableHeaders } from '@infrastructure/interfaces/table';
import {
  ADMIN_CHILDREN_TITLE,
  BTN_STYLE,
} from '@infrastructure/variants/styles';
import { ClientResponse } from 'types/client_response_schema';
import { UtilsService } from '@app/service/utils.service';
import { FormContainerComponent } from '@shared/form/form-container.component';
import { select, Store } from '@ngrx/store';
import { modifyClient } from '@store/actions/form/form.actions';
import { getClient } from '@store/selectors/form.selectors';
import { defaultClient } from '@infrastructure/constants/default-values';
import { initClients } from '@store/actions/client/client.actions';
import { selectState } from '@store/reducers/client.reducer';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.scss'],
})
export class ClientsComponent implements OnInit {
  protected readonly tableClientHeaders: TableHeaders[] = tableClientHeaders;
  protected readonly mapClientHeaders: JsonKeyHeaders[] = mapClientHeaders;
  protected readonly ADMIN_CHILDREN_TITLE: string = ADMIN_CHILDREN_TITLE;
  protected readonly BTN_STYLE: string = BTN_STYLE;

  public clients: ClientResponse[] = [];
  public client: ClientResponse = defaultClient;
  public unfilteredClients: ClientResponse[] = [];
  public open: boolean = false;
  public currentPage: number = 1;
  public loading: boolean = false;
  public itemsPerPage: number = 5;
  public pages: number[] = [];

  constructor(
    private _clientService: ClientsService,
    private _store: Store,
  ) {}

  ngOnInit() {
    this.getClients();
    this._store.pipe(select(getClient)).subscribe((client) => {
      this.client = client;
    });
    this._store.pipe(select(selectState)).subscribe(({ loading, clients }) => {
      this.unfilteredClients = clients;
      this.clients = clients.slice(0, this.itemsPerPage);
      this.loading = loading;
      this.pages = UtilsService.getPages(
        this.itemsPerPage,
        this.unfilteredClients,
      );
    });
  }

  getClients() {
    this._store.dispatch(initClients());
  }
  onEditRow(client: ClientResponse): boolean {
    this._store.dispatch(modifyClient(client));
    return (this.open = true);
  }
  closeDrawer(value: boolean): boolean {
    this.client = defaultClient;
    return (this.open = value);
  }
  filterQuery(value: string): ClientResponse[] {
    if (value === '') {
      return (this.clients = UtilsService.getCurrentItems(
        this.itemsPerPage,
        this.currentPage,
        this.unfilteredClients,
      ));
    }

    return (this.clients = this.clients.filter((client: ClientResponse) =>
      client.names.toLowerCase().includes(value.toLowerCase()),
    ));
  }
  filterItemsPerPage(itemsPerPage: number): ClientResponse[] {
    this.pages = [];
    this.itemsPerPage = itemsPerPage;
    this.pages = UtilsService.getPages(itemsPerPage, this.unfilteredClients);
    return (this.clients = UtilsService.getCurrentItems(
      this.itemsPerPage,
      this.currentPage,
      this.unfilteredClients,
    ));
  }
  pageChange(page: number): void {
    this.currentPage = page;
    this.clients = UtilsService.getCurrentItems(
      this.itemsPerPage,
      this.currentPage,
      this.unfilteredClients,
    );
  }
  createClient(frmComponent: FormContainerComponent): void {
    const request: ClientResponse = frmComponent.form!.value as ClientResponse;

    this._clientService
      .createClient(request)
      .subscribe(() => (this.open = false));
  }
}
