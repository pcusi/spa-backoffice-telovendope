import { NgModule } from '@angular/core';
import { CommonModule, NgOptimizedImage } from '@angular/common';
import { DashboardComponent, ClientsComponent } from '@app/admin';
import { RouterModule } from '@angular/router';
import { AdminComponent } from '@app/admin';
import { SharedModule } from '@shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { AddClientComponent } from './clients/add-client/add-client.component';
import { provideStore } from '@ngrx/store';
import { provideEffects } from '@ngrx/effects';
import { ClientEffects } from '@store/effects/client.effects';

@NgModule({
  declarations: [
    AdminComponent,
    DashboardComponent,
    ClientsComponent,
    AddClientComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    NgOptimizedImage,
    SharedModule,
    ReactiveFormsModule,
  ],
  bootstrap: [AdminComponent],
  providers: [provideStore(), provideEffects(ClientEffects)],
})
export class AdminModule {}
