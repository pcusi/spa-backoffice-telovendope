import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthComponent } from '@app/auth/auth.component';
import { SignInComponent } from '@app/auth/sign-in/sign-in.component';
import { AuthModule } from '@app/auth/auth.module';
import {
  AdminComponent,
  ClientsComponent,
  DashboardComponent,
} from '@app/admin';
import { AdminModule } from '@app/admin/admin.module';
import { AuthGuard } from '@app/auth/auth.guard';
import { StoreModule } from '@ngrx/store';
import { authFeature } from '@store/reducers/auth.reducer';
import { reactiveFormFeature } from '@store/reducers/form.reducer';
import { clientFeature } from '@store/reducers/client.reducer';

const routes: Routes = [
  {
    path: 'auth',
    component: AuthComponent,
    children: [{ path: 'sign-in', component: SignInComponent }],
  },
  {
    path: 'admin',
    component: AdminComponent,
    canActivate: [AuthGuard],
    children: [
      { path: 'dashboard', component: DashboardComponent },
      {
        path: 'clientes',
        component: ClientsComponent,
        data: { breadcrumb: '' },
      },
    ],
    data: { breadcrumb: '' },
  },
  {
    path: '',
    redirectTo: '/auth/sign-in',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    AuthModule,
    AdminModule,
    StoreModule.forRoot({
      auth: authFeature.reducer,
      form: reactiveFormFeature.reducer,
      client: clientFeature.reducer,
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
