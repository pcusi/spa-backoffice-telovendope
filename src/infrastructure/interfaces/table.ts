export interface TableHeaders {
  name: string;
  type: string;
}
export interface JsonKeyHeaders {
  keys: JsonKey[];
}
export interface JsonKey {
  key: string;
}
export interface CellJsonData {
  data: any;
  keys: JsonKey[];
}
