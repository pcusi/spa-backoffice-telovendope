export enum TableClientHeaders {
  NAMES = 'names',
  LASTNAMES = 'lastnames',
  CREATED_AT = 'created_at',
  ID = 'id'
}
