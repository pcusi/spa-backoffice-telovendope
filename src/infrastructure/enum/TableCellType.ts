export enum TableCellType {
  CHIP = 'chip',
  NORMAL = 'normal',
  OPTION = 'option',
  THUMBNAIL = 'thumbnail',
}
