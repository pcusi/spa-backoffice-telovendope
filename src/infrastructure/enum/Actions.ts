export enum ActionsType {
  AUTH = '[Auth] Authentication',
  AUTH_LOADING = '[Auth] Authentication loading',
  MODIFY_CLIENT = '[Form] Modify client',
  INIT_CLIENTS = '[Clients] Init clients',
  LOADING_CLIENTS = '[Clients] List clients loading',
  LIST_CLIENTS = '[Clients] List clients',
}
