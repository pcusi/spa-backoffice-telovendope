export const INPUT_STYLE: string =
  'px-4 py-3 rounded-xl border border-slate-300 bg-slate-200/25 text-sm transition ease-in-out delay-50 focus:bg-transparent focus:ring-0 focus:outline-none focus:text-slate-900';
export const INPUT_LABEL_STYLE: string =
  'block text-sm font-thin text-slate-900';
export const INPUT_FORM_GROUP_STYLE: string = 'flex flex-col gap-1';
export const BTN_STYLE: string =
  'px-4 py-3 bg-blue-500 rounded-md transition text-center text-white flex flex-row gap-2 items-center justify-center text-center hover:bg-blue-700 disabled:opacity-75 disabled:cursor-not-allowed';
export const ADMIN_CHILDREN_TITLE: string =
  'text-slate-900 font-semibold text-lg';
export const TABLE_OPTION_ACTION: string =
  'hover:text-blue-400 text-black transition ease-in cursor-pointer';
export const TABLE_OPTION_DELETE: string =
  'hover:text-red-400 text-black transition ease-in cursor-pointer';
export const INPUT_SELECT_STYLE: string =
  'bg-transparent border border-gray-200 px-4 py-2 min-w-[100px] rounded-md focus:outline-none  focus:bg-transparent focus:ring-0 focus:outline-none focus:text-slate-900';
