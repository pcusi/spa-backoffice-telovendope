import { JsonKeyHeaders } from '@infrastructure/interfaces/table';

export const mapClientHeaders: JsonKeyHeaders[] = [
  {
    keys: [{ key: 'names' }],
  },
  {
    keys: [{ key: 'lastnames' }],
  },
  {
    keys: [{ key: 'created_at' }],
  },
  {
    keys: [{ key: 'id' }],
  },
];
