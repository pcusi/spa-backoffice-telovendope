import { TableHeaders } from '@infrastructure/interfaces/table';
import { TableClientHeaders } from '@infrastructure/enum/TableHeaders';
import { clientHeadersES } from '@infrastructure/constants/es-headers';
import { TableCellType } from '@infrastructure/enum/TableCellType';
export const tableClientHeaders: TableHeaders[] = [
  {
    name: clientHeadersES[TableClientHeaders.NAMES],
    type: 'normal',
  },
  {
    name: clientHeadersES[TableClientHeaders.LASTNAMES],
    type: 'normal',
  },
  {
    name: clientHeadersES[TableClientHeaders.CREATED_AT],
    type: 'normal',
  },
  {
    name: clientHeadersES[TableClientHeaders.ID],
    type: TableCellType.OPTION,
  },
];
