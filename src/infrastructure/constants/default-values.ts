import { ClientResponse } from 'types/client_response_schema';

export const defaultClient: ClientResponse = {
  names: '',
  lastnames: '',
};
