export interface MenuOption {
  name: string;
  icon: string;
  redirectTo: string;
}
export const sidebar: MenuOption[] = [
  {
    name: 'Dashboard',
    icon: 'heroChartBarSquare',
    redirectTo: '/admin/dashboard',
  },
  {
    name: 'Clientes',
    icon: 'heroUsers',
    redirectTo: '/admin/clientes',
  },
  {
    name: 'Pedidos',
    icon: 'heroCircleStack',
    redirectTo: '/admin/pedidos',
  },
  {
    name: 'Ajustes',
    icon: 'heroCog6Tooth',
    redirectTo: '/admin/ajustes',
  },
  {
    name: 'Mi tienda',
    icon: 'heroBuildingStorefront',
    redirectTo: '/admin/mi-tienda',
  },
];
