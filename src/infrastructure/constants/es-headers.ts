import { TableClientHeaders } from '@infrastructure/enum/TableHeaders';
export const clientHeadersES: Record<TableClientHeaders, string> = {
  [TableClientHeaders.NAMES]: 'Nombres',
  [TableClientHeaders.LASTNAMES]: 'Apellidos',
  [TableClientHeaders.CREATED_AT]: 'Fecha creación',
  [TableClientHeaders.ID]: 'Opciones',
};
